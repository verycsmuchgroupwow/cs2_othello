Yan bing wrote out the code for the minimax the first week, while
Caitlin helped with the debugging.
The following week Caitlin and Yan Bing both tried to fix the minimax
algorithm that wasn't implemented correctly and each tried their own
way of implementing it. Yan Bing attempted a more weighted version
that proved to make progress so they used her code.

Improvements:
Added weighted calculations to favor corners that improved the rate of
winning. The strategy should work because it favors corners, Although some of the other weighted calculations that favored near the corners
actually made our AI perform worse. We are not sure why this happened
We tried to implement the minimax recursively, however that did not
turn out well and led to a seg fualt. 
