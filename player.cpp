#include "player.h"
#include <vector>
#include <climits>
#include <cmath>
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    myside = side;
    if (myside == WHITE) {
		opponentside = BLACK;
	}
	else {
		opponentside = WHITE;
	}
    myboard = new Board();

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {


	//int alpha = INT_MIN;
	//int beta = INT_MAX;
	//does opponents move
	myboard->doMove(opponentsMove, opponentside);
    
    if (myboard->hasMoves(myside) == false) {
		return NULL;
	}
	
	vector<Move*> moves;
	for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move *move = new Move(i, j);
            if (myboard->checkMove(move, myside)) {
				moves.push_back(move);
			}
        }
    }

    vector<int> scores; //even index is score, odd index is corresponding index in moves
    for (unsigned int x = 0; x < moves.size(); x++) {
		Board *temp = myboard->copy();
		temp->doMove(moves[x], myside);
		int worsecount = INT_MAX;
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				Move *move = new Move(i, j);
				if (temp->checkMove(move, opponentside)) {
					Board *temp2 = temp->copy();
					temp2->doMove(move, opponentside);
					int count = temp2->countWhite() - temp2->countBlack();
					if (myside == BLACK) {
						count *= -1;
					}
					if (count < worsecount) {
						worsecount = count;
					}
								
				}
			}
		}
		if (worsecount != INT_MAX) {
			scores.push_back(worsecount);
			scores.push_back(x);		
		}
	}
	int bestScore;
	int bestMove = 0;
	if (!scores.empty()) {
		bestScore = scores[0];
		bestMove = scores[1];
	}
	for (unsigned int i = 0; i < scores.size(); i+=2) {
		if (scores[i] > bestScore) {
			bestScore = scores[i];
			bestMove = scores[i+1];
		}
		if (scores[i] == bestScore) {
			int x = moves[scores[i+1]]->getX();
			int y = moves[scores[i+1]]->getY();
			int origx = moves[bestMove]->getX();
			int origy = moves[bestMove]->getY();
			if (x == y and (x == 0 or x == 7)) {
				bestScore = scores[i];
				bestMove = scores[i+1];
			}
			else if (origx != 0 or origx == 7 or origy != 0 or origy != 7) {
				if (x == 0 or x == 7 or y == 0 or y == 7) {
					if (std::abs(x -y) != 1 and std::abs(x -y) != 6) {
						bestScore = scores[i];
						bestMove = scores[i+1];
					}
				}				
			}
		}
	}
	
	myboard->doMove(moves[bestMove], myside);
	
	Move *finalmove = moves[bestMove];
	moves.erase(moves.begin(), moves.end());
	scores.erase(scores.begin(), scores.end());
	
	return finalmove;
}

